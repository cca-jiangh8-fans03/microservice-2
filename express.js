const express = require('express')
const app = express()
var port = process.env.PORT || 8000
app.use(express.urlencoded({extended:false}))
const cors = require('cors')
app.use(cors())
app.listen(port, ()=>
    console.log(`HTTP Server with express.js is listening on port:${port}`)
)
app.get('/',(req,res)=>{
    res.send('Microservice 2 Gateway by Hanwen Jiang and Siqiang Fan. Usage: host/datediff?from_date=mm/dd/yyyy&to_date=mm/dd/yyyy')
})
app.get('/datediff', function(req,res){
    from_date = Date.parse(req.query['from_date'])
    to_date = Date.parse(req.query['to_date']) 
    if( isNaN(from_date) || isNaN(to_date)){
        res.send("Invalid inputs. Use date in mm/dd/yyyy or Month dd, yyyy")
        return
    }
    
    var diffDays = parseInt((to_date - from_date)/(24*60*60*1000));
    res.send(`${diffDays} days or ${Number(diffDays/7).toFixed(1)} weeks`)
})
